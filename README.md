![Freelo logo](https://bitbucket.org/data-based/ex-freelo/raw/905f96ec3b0331d5ab1bf40882cead928c7d68ab/freelo-logo-rgb.png)

# Freelo API Extractor
Extracts data of a user (owned and invited projects) from Freelo API.

Freelo is great project management and collaboration platform. 
Haven't you try Freelo yet? [Try free trial](https://www.freelo.cz/en)

### Instructions
1. Create a new configuration
2. Replace `username` (email) and provided `api key` from [Settings](https://app.freelo.cz/profil/nastaveni)
3. Select a template
4. Run and schedule the extractor

### Freelo links
* [API docs](https://freelo.docs.apiary.io/)
* [Help](https://help.freelo.cz/en/)
* [Useful tips](https://www.freelo.cz/en/webinars)

**Any questions or missing anything?** Let us know
[team@data-based.io](mailto:team@data-based.io?subject=Keboola%20Freelo%20extractor)